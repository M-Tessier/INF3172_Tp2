/**
 * fin.c
 *
 * This program diplay the last X line of a text file.
 *
 * @author Michael Tessier (TESM17069209)
 * @version 1.0
 * @since 2017-4-20
 */
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
 
    if(argc == 1 || argc == 2 || argc > 3)
        fprintf(stderr, "fin need 2 arguments");
    
    FILE *ptr_file;
    int x;

    ptr_file = fopen(argv[2], "r");

    if(!ptr_file) {
        fprintf(stderr, "Fichier Introuvable");
        return 1;
    }
    
    int nb = atoi( argv[1]);
    char stringTable[1024][1024];

    int i;
    for(i = 0; i < 128; ++i) {
        
        fgets(stringTable[i], 1024, ptr_file);
        
        if(feof(ptr_file))
            break;
    }

    int index = i - nb;

    int j;
    for(j = 0; j < nb; ++j)
        fprintf(stdout, "%s", stringTable[index+j]);

    return 0;
}
