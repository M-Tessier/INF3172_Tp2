/**
 * size.c
 *
 * This program display the number of files a specified directory
 * and the size of this directory.
 *
 * @author Michael Tessier (TESM17069209)
 * @version 1.0
 * @since 2017-4-20
 */

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

static int nbFile = 0; 
static int total = 0;

int sum(const char *fpath, const struct stat *sb, int typeflag) {
    ++nbFile;
    total += sb->st_size;
    return 0;
}

int main(int argc, char **argv) {
     
    if(argc == 1) {
        fprintf(stderr, "size need an argument\n");
        return 1;
    }      
    else if(argc > 2) {
        fprintf(stderr, "size need a single argument\n");
        return 1;
    }
    
    if(!argv[1] || access(argv[1], R_OK)) {
        fprintf(stderr, "Repertoire not found\n");
        return 1;
    }

    if(ftw(argv[1], &sum, 1)) {
        perror("ftw");
        return 1;
    }

    fprintf(stdout, "The directory %s has %d files for a total of %d bytes.\n", 
        argv[1], nbFile-1, total);

    return 0;
}
