CC = gcc
CC_FLAGS = -w
TSH = tsh
NEW = new
NEWDIR = newdir
SIZE = size
FIN = fin
RMALL = rmall
BIN = inf3172/bin/
SOURCES = $(wildcard *.c)
OBJECTS = $(SOURCES:.c=.o)

.PHONY: default all clean

default: $(TSH)
all: $(TSH) $(NEW) $(NEWDIR) $(SIZE) $(FIN) $(RMALL)
 
$(TSH): shell.o main.o
	@mkdir -p inf3172/bin
	$(CC) shell.o main.o -o $(BIN)$(TSH)

$(NEW): new.o
	$(CC) new.o -o $(BIN)$(NEW)

$(NEWDIR): newdir.o
	$(CC) newdir.o -o $(BIN)$(NEWDIR)

$(SIZE): size.o
	$(CC) size.o -o $(BIN)$(SIZE)

$(FIN): fin.o
	$(CC) fin.o -o $(BIN)$(FIN)

$(RMALL): rmall.o
	$(CC) rmall.o -o $(BIN)$(RMALL)

%.o: %.c
	$(CC) -c $(CC_FLAGS) $< -o $@
 
clean:
	rm -f $(BIN)$(TSH) $(BIN)$(NEW) $(BIN)$(NEWDIR) $(BIN)$(SIZE) $(BIN)$(FIN) $(BIN)$(RMALL) $(OBJECTS)
 
