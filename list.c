/**
 * list.c
 *
 * This program list the content of a specified directory.
 *
 * @author Michael Tessier (TESM17069209)
 * @version 1.0
 * @since 2017-4-20
 */

#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>

int main( int argc, char *argv[]) {
	
	DIR *dirp;
	struct dirent *dp;

	if((dirp = opendir(".")) == NULL) {
		perror("couldn't open '.'");
		return 1;
	}

	
}
