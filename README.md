# Travail pratique 2


## Description

This program, written in C language, is a simple shell able to navigate through 
the linux file system and execute programs stored in the ./inf3172/bin folder.

Multiple small programs were added to ./inf3172/bin to test the shell.

The project was created as part of the course INF3172 "Principes des systemes 
d'exploitation" during the winter semester of 2017 at the l'Université du Québec à 
Montréal.


## Authors


- Michael Tessier (TESM17069209)


## Supported Plateforms


* Ubuntu 16.04 LTS: Fully supported.
* MALT server: Fully supported.


## Installation


### make

To generate the executable for the shell, "tsh":

    $ make

### make all

To generate executables for every program:

    $ make all


## Bibliography

* https://brennan.io/2015/01/16/write-a-shell-in-c/
* http://www.puxan.com/web/howto-write-generic-makefiles/
* http://linuxgazette.net/111/ramankutty.html
* https://www.codingunit.com/c-tutorial-file-io-using-text-files
* http://www.csl.mtu.edu/cs4411.ck/www/NOTES/make/multiple.html
* http://stackoverflow.com/questions/7430248/creating-a-new-directory-in-c
* http://stackoverflow.com/questions/472697/how-do-i-get-the-size-of-a-directory-in-c
* http://stackoverflow.com/questions/2256945/removing-a-non-empty-directory-programmatically-in-c-or-c
