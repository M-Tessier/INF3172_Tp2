/**
 * main.c
 *
 * @author Michael Tessier (TESM17069209)
 * @version 1.0
 * @since 2017-4-20
 */

#include "shell.h"

int main(int argc, char *argv[])
{
    shellLoop();

    return EXIT_SUCCESS;
}
