/**
 * shell.h
 *
 * The shell module, made of shell.h and shell.c implement
 * the shell loop, the functions to interprete the commands
 * and exectute the other small programs
 *
 * @author Michael Tessier (TESM17069209)
 * @version 1.0
 * @since 2017-4-20
 */

#ifndef _SHELL_H_
#define _SHELL_H_

#include <linux/limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

void shellLoop();
char* readCommand();
char** tokenizeArguments(char*);

extern const char *directCommands[];

extern const int nbDirectCommands();


int tsh_cd(char**);
int tsh_cdir(char**);
int tsh_exit(char**);

extern int (*directFunctions[]) (char**);

int launchCommand(char**);
int executeCommand(char**);


#endif
