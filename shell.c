/**
 * shell.c
 *
 * @author Michael Tessier (TESM17069209)
 * @version 1.2
 * @since 2017-4-20
 */

#include "shell.h"

const char *directCommands[] = {
    "cd",
    "cdir",
    "exit"
};

const int nbDirectCommands() {
    return sizeof(directCommands) / sizeof(char*);
}

void shellLoop()
{
    char *command;
    char **argumentTable;
    int status;

    do{
        printf("tsh>");
        command = readCommand();
            argumentTable = tokenizeArguments(command);
            status = executeCommand(argumentTable);
        
            free(command);
            free(argumentTable);
    } while(status);
}

char* readCommand()
{
    char *command = NULL;
    ssize_t commandSize = 0;
    getline(&command, &commandSize, stdin);
    return command;
}

char** tokenizeArguments(char* command) 
{
    int index = 0;
    char *argument;
    char **argumentTable = malloc(sizeof(char) * 128);

    argument = strtok(command, " \n");
    while(argument != NULL) {
        argumentTable[index] = argument;
        argument = strtok(NULL, " \n");
        ++index;
    }

    argumentTable[index] = NULL;
    return argumentTable;
}

int tsh_cd(char **argumentTable ) {
    if(argumentTable[1] == NULL)
        fprintf(stderr, "tsh: cd need an argument\n");
    else
        if(chdir(argumentTable[1]) == -1)
            perror("tsh");

    return 1;
}

int tsh_cdir(char **argumentTable) {
    char *path = malloc(sizeof(char) * 128);

    if(getcwd(path, sizeof(char) * 128) == NULL) {
        perror("tsh");
        exit(EXIT_FAILURE);
    }
    else
        fprintf(stdout, "Repertoire courant : %s\n", path);

    return 1;
}

int tsh_exit(char **argumentTable) {

    return 0;
}

int (*directFunctions[]) (char**) = {
    &tsh_cd,
    &tsh_cdir,
    &tsh_exit
};

int launchCommand(char **argumentTable)
{
    char path[32] = "./inf3172/bin/";
    strcat(path, argumentTable[0]);
    pid_t pid, wpid;
    int status;

    pid = fork();
    if (pid == 0) {
            if(execv(path, argumentTable) == -1)
                perror("tsh");
        exit(1);
    }
    else if (pid < 0)
        perror("tsh");
    else
        do {
            wpid = waitpid(pid, &status, WUNTRACED);
        } while (!WIFEXITED(status) && !WIFSIGNALED(status));

    return 1;
}

int executeCommand(char** argumentTable) 
{
    if(argumentTable[0] == NULL )
        return 1;
    
    int i;
    for(i = 0; i < nbDirectCommands(); i++) {
        if(strcmp(argumentTable[0], directCommands[i]) == 0)
            return (*directFunctions[i])(argumentTable);
    }

    return launchCommand(argumentTable);
}
