/**
 * new.c
 *
 * This program create a new text file at a specified path.
 *
 * @author Michael Tessier (TESM17069209)
 * @version 1.0
 * @since 2017-4-20
 */

#include <stdio.h>

int main(int argc, char *argv[]) {
    
    if(argc == 1)
        fprintf(stderr, "New need an argument");
    else if(argc > 2)
        fprintf(stderr, "New need a single argument");

    FILE *ptr_file;
    ptr_file = fopen(argv[1], "w");

    if(!ptr_file) {
        fprintf(stderr, "Impossible to create file\n");
        return 1;
    }
    else
        fprintf(stdout, "File created\n");

    fclose(ptr_file);

    return 0;
}
