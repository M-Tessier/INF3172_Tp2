/**
 * newdir.c
 *
 * This program a new directory at a specified path.
 *
 * @author Michael Tessier (TESM17069209)
 * @version 1.0
 * @since 2017-4-20
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>

struct stat st = {0};

int main(int argc, char *argv[]) {
    
    if(argc == 1)
        fprintf(stderr, "newdir need and argument\n");
    else if(argc > 2)
        fprintf(stderr, "newdir need a single argument\n");

    if(mkdir(argv[1], S_IRWXU) == -1)
        fprintf(stderr, "Impossible to create directory\n");

    return 0;
}
