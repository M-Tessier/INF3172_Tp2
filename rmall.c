/**
 * rmall.c
 *
 * This program delete a directory and it's content.
 *
 * @author Michael Tessier (TESM17069209)
 * @version 1.0
 * @since 2017-4-20
 */

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
int main(int argc, char **argv) {
     
    if(argc == 1) {
        fprintf(stderr, "size need an argument\n");
        return 1;
    }      
    else if(argc > 2) {
        fprintf(stderr, "size need a single argument\n");
        return 1;
    }
    
    if(!argv[1] || access(argv[1], R_OK)) {
        fprintf(stderr, "Repertoire not found\n");
        return 1;
    }

    if(ftw(argv[1], &remove, 1)) {
        perror("ftw");
        return 1;
    }

    return 0;
}
